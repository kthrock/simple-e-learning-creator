# README #

Simply E-Learning Creator

### What is this repository for? ###

* This is a prototype to help user's create their own e-learning modules without code or design skills.
* v1.00

### How do I get set up? ###

* Python 3.4
* To run the application in the terminal, run 'python main.py'
* Follow the command prompts for inputs
* The results will be stored in the 'Output' folder