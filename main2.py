# import os
#
# numChapter = input("How Many Chapters: ")
#
# if not os.path.exists('Output'):
#     os.makedirs('Output')
#
# for index in range(int(numChapter)):
#     f = open('Output/chapter'+str(index+1)+'.html', 'w')
#     title = input("Enter Title: ")
#     f.write('<DOCTYPE html> \n'
#             '<head>\n'
#             '<title>' + title + '</title> \n'
#             '<html>' + title + '</html>')
from app import *
import os
import re


s = open('input.txt', 'r')
data = s.read()
x = re.findall(r'~%(.*?)%~',data,re.DOTALL)
x2 = re.findall(r'alpha(.*?)bravo',data,re.DOTALL)
q = "".join(x).replace('\n', '')
q2 = "".join(x2).replace('\n', '')
x = int(q)
x2 = int(q2)
print(x)
print(x2)

ch = {}

numChapters = x


for k in range(numChapters):


    slide = {}

    numSlides = x2

    for index in range(numSlides):
        slide[index] = Slide(input('What is the name of Slide'+str(index+1) + '?\n'), input('What is the text?\n'))

    for j in range(numSlides):
        print('Title: ' + slide[j].name + '\nText: ' + slide[j].text + '\n')

    if not os.path.exists('Output'):
        os.makedirs('Output')
    i = 0
    for i in range(int(numSlides)):
        newpath = 'Output/chapter'+str(k+1)
        print(newpath)
        if not os.path.exists(newpath):
            os.makedirs(newpath)
        f = open(str(newpath) +'/slide'+str(i+1)+'.html', 'w')
        f.write('<DOCTYPE html> \n'
                '<html>\n'
                '<head>\n'
                '<title>' + slide[i].name + '</title> \n'
                '<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">\n'
                '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>\n'
                '<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>\n'
                '<script src="../../build/webspeech.js"></script>\n'

                '<style>\n'
                '   div{background-color: #ffffff;}\n'
                '</style>\n'
                '</head>\n'
                '<body style="background-color: #000000">\n'
                '<div class="container-fluid" style="max-width: 960px;">\n'
                '   <h3>Logo</h3>\n'
                '    <div class="row">\n'
                '        <div class="col-xs-12" style="background-color: #696969; height: 25px">\n'
                '            <h5 class="pull-right"style="color: white; margin-top: 5px; margin-right: 10px;">\n'
                '             Page ' + str(i+1)+ ' of ' + str(numSlides) + '\n'
                '            </h5>\n'
                '        </div>\n'
                '    </div>\n'
                '<!-- Module Start -->\n'
                '<div class="row" style="height: 420px">\n'
                '   <div class="col-sm-12" style="height: 30px"></div>\n'
                '   <div class="col-sm-5 content-text">\n'
                '       <h4>'+slide[i].name+'</h4>\n'
                '       <p id="text" style="text-align: justify">'+slide[i].text+'</p>\n'
                '   </div>\n'
                '   <div class="col-sm-5 content-image">\n'
                '   <img src="../resources/imagetest.jpg" />\n'
                '   </div>\n'
                '   <div class="col-sm-12" style="height: 20px"></div>\n'
                '   <div class="col-sm-12" style="background-color: lightgrey; height: 5px"></div>\n'
                '   <div class="col-sm-12" style="background-color: #696969; height: 75px">\n'

                '   <button type="button" class="btn btn-default pull-right" style="margin: 10px;" onclick=next()>\n'
                '       Next\n'
                '   </button>\n'
                '   <button type="button" class="btn btn-default pull-right" style="margin: 10px;" onclick=back()>\n'
                '       Back\n'
                '   </button>\n'
                '   <button id="playaudio" type="button" class="btn btn-default pull-right" style="margin: 10px;" onclick=talk()>\n'
                '       Play Audio\n'
                '   </button>\n'
                '   <button id="stopaudio" type="button" class="btn btn-default pull-right" style="margin: 10px; display: none;" onclick=stoptalk()>\n'
                '       Stop Audio\n'
                '   </button>\n'
                '   <button id="pauseaudio" type="button" class="btn btn-default pull-right" style="margin: 10px; display: none;" onclick=pausetalk()>\n'
                '       Pause Audio\n'
                '   </button>\n'
                '   <button id="resumeaudio" type="button" class="btn btn-default pull-right" style="margin: 10px; display: none;" onclick=resumetalk()>\n'
                '       Resume Audio\n'
                '   </button>\n'
                #'   <button type="button" class="btn btn-default pull-right" style="margin: 10px;"
                # onclick="mainmenuButton()">Main Menu</button>
                '   </div>\n'
                '</div>\n'
                '<!-- Module End -->\n'

                '<script>\n'
                'function next(){\n'
                '   if('+str(i+1)+'<'+str(numSlides)+'){\n'
                '       window.location.href="slide'+str(i+2)+'.html"\n'
                '   }\n'
                '}\n'
                'function back(){\n'
                '   if('+str(numSlides)+'>'+str(numSlides - i)+'){\n'
                '       window.location.href="slide'+str(i)+'.html"\n'
                '   }\n}\n'
                'var speaker;\n'
	            'try{\n'
		        'speaker = new RobotSpeaker();\n'
	            ' }\n'
	            'catch(ex){\n'
		        'speaker = null;\n'
                '}\n'
                'function talk() {\n'
		        'if(speaker){\n'
			    'speaker.speak("en", document.getElementById("text").innerHTML);\n'
                'document.getElementById("playaudio").style.display = "none";\n'
                'document.getElementById("pauseaudio").style.display = "block";\n'
                '}\n'
                '}\n'
                'function stoptalk(){\n'
                'speaker.cancel();\n'
                '}\n'
                'function pausetalk(){\n'
                'speaker.pause();\n'
                'document.getElementById("resumeaudio").style.display = "block";\n'
                'document.getElementById("pauseaudio").style.display = "none";\n'
                '}\n'
                'function resumetalk(){\n'
                'speaker.resume();\n'
                'document.getElementById("resumeaudio").style.display = "none";\n'
                'document.getElementById("pauseaudio").style.display = "block";\n'
                '}\n'
                '</script>\n'
                '</body>\n'
                '</html>')
        f.close()